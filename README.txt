INSTALLING COMMERCE CHECKOUT.FI MODULE
-------------------------------------

1. Install the module.

2. Make sure cron is running frequently. At least once an hour is recommended.


CONFIGURING PAYMENT METHOD
--------------------------

All payment method settings are in the Checkout.fi
payment method rule configuration page.

1. Go to Checkout.fi payment method rule config:
   admin/config/workflow/rules/reaction/manage/commerce_payment_checkoutfi

2. Edit action: "Enable payment method: Checkout.fi"

3. Add your custom configurations or use the predefined test accounts.


CREDITS
-------

Commerce Checkout.fi integration has been developed by Olli Erinko and sponsored
by Mearra. Drupal 9 implementation has been developed by Jyri-Petteri Paloposki
and sponsored by Ardcoras oy.
