<?php

namespace Drupal\commerce_checkout_fi\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Checkout.fi off-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "checkoutfi_payment_gateway",
 *   label = @Translation("Checkout.fi"),
 *   display_label = @Translation("Checkout.fi"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_checkout_fi\PluginForm\OffsiteRedirect\CheckoutFiPaymentForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class CheckoutFiGateway extends OffsitePaymentGatewayBase {

  use LoggerChannelTrait;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ID of the parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var string
   */
  protected $entityId;

  /**
   * The payment type used by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeInterface
   */
  protected $paymentType;

  /**
   * The payment method types handled by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeInterface[]
   */
  protected $paymentMethodTypes;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'passphrase' => '',
      'language' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credentials'),
      '#description' => $this->t('The credentials are supplied by Checkout.fi. If you don\'t have them, contact the Checkout customer service.'),
    ];
    $form['credentials']['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant account number'),
      '#default_value' => (!empty($this->configuration['merchant_id']) ? $this->configuration['merchant_id'] : 375917),
      '#description' => $this->t('For testing use %test_merchant_id', ['%test_merchant_id' => 375917]),
    ];
    $form['credentials']['passphrase'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Secret passphrase"),
      '#default_value' => (!empty($this->configuration['passphrase']) ? $this->configuration['passphrase'] : 'SAIPPUAKAUPPIAS'),
      '#description' => $this->t('For testing use %test_passphrase', ['%test_passphrase' => 'SAIPPUAKAUPPIAS']),
    ];

    $form['language'] = [
      '#type' => 'select',
      '#title' => t('Language preference'),
      '#options' => [
        'FI' => t('Finnish'),
        'SE' => t('Swedish'),
        'EN' => t('English'),
      ],
      '#default_value' => (!empty($this->configuration['language']) ? $this->configuration['language'] : 'FI'),
    ];

    $form['submit_address'] = [
      '#type' => 'checkbox',
      '#title' => t('Submit address to Checkout'),
      '#default_value' => (!empty($this->configuration['submit_address']) ? $this->configuration['submit_address'] : FALSE),
    ];

    $form['delivery_date'] = [
      '#type' => 'textfield',
      '#title' => t('Delivery date'),
      '#description' => t('English textual datetime description, which gets converted into unix timestamp by PHP\'s <code>strtotime()</code> function.'),
      '#default_value' => !empty($this->configuration['delivery_date']) ? $this->configuration['delivery_date'] : 'now +30 day',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->getErrors()) {
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['credentials']['merchant_id'];
      $this->configuration['passphrase'] = $values['credentials']['passphrase'];
      $this->configuration['language'] = $values['language'];
      $this->configuration['submit_address'] = $values['submit_address'];
      $this->configuration['delivery_date'] = $values['delivery_date'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    if ($feedback = $this->getFeedback()) {
      // Check that we have a valid hmac hash in the callback.
      if ($this->validHmacCallback($order, $feedback) && $feedback['checkout-status'] == 'ok') {
        /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

        $payment = $payment_storage->create([
          'state' => 'completed',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->entityId,
          'order_id' => $order->id(),
          'remote_id' => $feedback['checkout-transaction-id'],
          'remote_state' => $feedback['checkout-status'],
        ]);
        $payment->save();
        return TRUE;
      }
      else {
        throw new PaymentGatewayException('Order status not ready.');
      }
    }
    else {
      throw new PaymentGatewayException('Missing the feedback parameters.');
    }
  }

  /**
   * Get Checkout.fi feedback from GET/POST parameters
   */
  private function getFeedback() {
    $feedback = FALSE;
    if (isset($_REQUEST['signature'])) {
      $feedback = [];
      // Prepare the feedback values sent by Checkout.fi for processing.
      // $_REQUEST since this includes the $_SESSION variables.
      // FIXME: Does this need sanisation?
      foreach ($_GET + $_POST as $key => $value) {
        if (strpos($key, 'checkout-') === 0) {
          $feedback[$key] = $value;
        }
      }
      ksort($feedback);
    }
    return $feedback;
  }

  /**
   * Check if HMAC-SHA256 hash in feedback is valid
   */
  private function validHmacCallback($order, $feedback) {
    $transactionId = $order->getData('checkout_fi_transaction_id');

    // Check if the received HMAC hash is valid.
    if (!$this->validHmac($feedback, $_GET['signature']) || !$transactionId || $transactionId !== $feedback['checkout-transaction-id']) {
      \Drupal::messenger()->addError('Failed to verify the payment status. Please contact out customer support.');
      \Drupal::logger('commerce_checkout_fi')->error("HMAC signature doesn't match for order %order! Got %signature.", ['%order' => $order->id(), '%signature' => $_GET['signature']]);
      throw new PaymentGatewayException('HMAC validation failed.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get the HMAC-SHA256 hash and compare it to the one we got from Checkout.fi.
   */
  private function validHmac($feedback, $signature) {
    $secret = $this->configuration['passphrase'];

    $hash = $this->calculateHmac($secret, $feedback, '');
    return $hash === $signature;
  }

  /**
   * Calculate the HMAC-SHA256 hash.
   */
  function calculateHmac($secret, $params, $body = '') {
    // Keep only checkout- params, more relevant for response validation. Filter
    // query string parameters the same way - the signature includes only
    // checkout- values.
    $includedKeys = array_filter(array_keys($params), function ($key) {
      return preg_match('/^checkout-/', $key);
    });

    // Keys must be sorted alphabetically
    sort($includedKeys, SORT_STRING);

    $hmacPayload = array_map(
      function ($key) use ($params) {
        return join(':', [ $key, $params[$key] ]);
      },
      $includedKeys
    );

    array_push($hmacPayload, $body);

    return hash_hmac('sha256', join("\n", $hmacPayload), $secret);
  }
}
