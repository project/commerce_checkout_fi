<?php

namespace Drupal\commerce_checkout_fi\PluginForm\OffsiteRedirect;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CheckoutFiPaymentForm extends BasePaymentOffsiteForm {
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $settings = $payment_gateway_plugin->getConfiguration();

    $order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $order = Order::load($order_id);

    $paymentRequest = new \OpMerchantServices\SDK\Request\PaymentRequest();
    $paymentRequest
      ->setReference($order_id)
      ->setAmount(round(number_format($payment->getAmount()->getNumber(), 2, '.', '') * 100))
      ->setCurrency($payment->getAmount()->getCurrencyCode());

    // URLs
    $success_url = Url::FromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order_id,
      'step' => 'payment'
    ], ['absolute' => TRUE])->toString();

    $cancel_url = Url::FromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order_id,
      'step' => 'payment'
    ], ['absolute' => TRUE])->toString();

    $callbackUrls = new \OpMerchantServices\SDK\Model\CallbackUrl();
    $callbackUrls->setSuccess($success_url);
    $callbackUrls->setCancel($cancel_url);
    $paymentRequest->setRedirectUrls($callbackUrls);
//    $paymentRequest->setCallbackUrls($callbackUrls);

    $paymentRequest->setLanguage($settings['language']);

    $address = $order->getBillingProfile()->address->first();

    $stamp = $settings['merchant_id'] . 'A' . (int) trim($order_id) . date('YmdHis');
    $paymentRequest->setStamp($stamp);

    $customer = new \OpMerchantServices\SDK\Model\Customer();
    $customer
      ->setFirstName($address->getGivenName())
      ->setLastName($address->getFamilyName())
      ->setEmail($order->getEmail());
    $paymentRequest->setCustomer($customer);

    if ($settings['submit_address']) {
      $api_address = new \OpMerchantServices\SDK\Model\Address();
      $api_address
        ->setCountry(t('Finland'))
        ->setCity($address->getLocality())
        ->setPostalCode($address->getPostalCode())
        ->setStreetAddress($address->getAddressLine1());
      $paymentRequest->setInvoicingAddress($api_address);
    }

    $items = [];
    $pr = 0;

    $delivery_date = date('Y-m-d', strtotime($settings['delivery_date']));
    foreach ($order->getItems() as $line_item) {
      $item = new \OpMerchantServices\SDK\Model\Item();
      if ($line_item->hasPurchasedEntity()) {
        $product = $line_item->getPurchasedEntity();
        $item->setProductCode($product->getSku());
      }
      else {
        $item->setProductCode('unknown');
      }

      $item->setUnits(intval($line_item->getQuantity()));
      $item->setUnitPrice(intval($line_item->getUnitPrice()->getNumber()*100));
      $item->setDescription($line_item->getTitle());
      $item->setDeliveryDate($delivery_date);
      $tax_adjustments = $line_item->getAdjustments(['tax']);
      if ($tax_adjustments) {
        $tax_rate = $tax_adjustments[0]->getPercentage();
        $item->setVatPercentage(intval($tax_rate * 100));
      }
      else {
        $item->setVatPercentage(0);
      }
      $items[] = $item;
    }

    if (!empty($order->shipments->entity)) {
      $shipment = $order->shipments->entity;
      $item = new \OpMerchantServices\SDK\Model\Item();
      $item->setProductCode('shipping');
      $rate = $shipment->getAmount()->multiply(100)->getNumber();
      // It seems that in some cases there are two shipping items even though
      // only one of those is reflected in the order total.
//      $item->setUnits($shipment->getTotalQuantity());
      $item->setUnits(1);
      $item->setUnitPrice(intval($rate));
      $item->setDescription('Shipping');
      $item->setDeliveryDate($delivery_date);

      // Tax
      $tax_adjustments = $line_item->getAdjustments(['tax']);
      if ($tax_adjustments) {
        $tax_rate = $tax_adjustments[0]->getPercentage();
        $item->setVatPercentage(intval($tax_rate * 100));
      }
      else {
        $item->setVatPercentage(0);
      }

      $items[] = $item;
    }

    $paymentRequest
      ->setItems($items);

//    echo '<pre>'; var_dump($paymentRequest); die();

    try {
      $client = new \OpMerchantServices\SDK\Client($settings['merchant_id'], $settings['passphrase'], 'drupal-commerce_checkout_fi-8.x');

      $payment = $client->createPayment($paymentRequest);
      $order->setData('checkout_fi_transaction_id', $payment->getTransactionId())
        ->save();

      $redirect_url = $payment->getHref();
      $redirect_method = BasePaymentOffsiteForm::REDIRECT_GET;
    } catch(\Exception $e) {
      \Drupal::messenger()->addError('There was an error moving to the payment service. Try again or contact customer service. Sorry for the inconvenience!');
      \Drupal::logger('commerce_checkout_fi')->error("Creating payment and redirect URL failed due to exception: %e_msg", ['%e_msg' => $e->getMessage()]);
      $checkout_url = \Drupal\Core\Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $order->id(),
        'step' => 'review',
      ])->toString();
      // FIXME: For some reason the correct (?) way to redirect didn't work.
//      return new RedirectResponse($checkout_url);
      header('Location: ' . $checkout_url);
      die();
    }
    return $this->buildRedirectForm($form, $form_state, $redirect_url, [], $redirect_method);
  }
}
